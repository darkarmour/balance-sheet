This is a Balance Sheet project bootstrapped with client [Next JS](https://nextjs.org/) and server [Express JS]running on Heroku with AWS services.
- Node version - v12.16.1

## Client
-   Front end of this project is developed using NextJS
-   Open [https://balance-sheet-ui-jp.herokuapp.com](https://balance-sheet-ui-jp.herokuapp.com) with your browser to see the result.

## Server
-   Back end of this project is developed using ExpressJS
-   Open [https://balance-sheet-api-jp.herokuapp.com]

## Database
- AWS DyanamoDB is used as data source for this project.

## Hosting
- Heroku is used for hosting for this project.