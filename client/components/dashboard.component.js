import React from "react";
import { Button, Form, FormGroup, Col, Alert, Table } from 'react-bootstrap';

class DashboardComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            reports: [],

            reportDate: undefined,
            reportTimeFrame: undefined,

            openedReport: undefined
        }
    }

    componentDidMount() {
        if (this.props && this.props.user) {
            let user = JSON.parse(JSON.stringify(this.props.user));
            this.setState({
                reports: user.reports,
                reportDate: user.reports[0].date,
                reportTimeFrame: user.reports[0].timeframe
            });
        }
    }

    handleSubmit(event) {
        event.preventDefault();
    }

    handleOnChange(event, id) {
        this.setState({ [id]: event.target.value })
    }

    handleGetReport() {
        let reports = this.state.reports || [];
        let openedReport = reports.find(i => (i.date === this.state.reportDate) && (i.timeframe === this.state.reportTimeFrame));
        console.log(openedReport)
        this.setState({
            openedReport: openedReport ? openedReport.report : undefined
        })
    }

    getTableHeaderRow(openedReport) {
        let headerRow = openedReport.rows.find(i => i.rowType === 'Header');
        let tableHeaderRow = headerRow.cells.map(i => {
            return <th>{i.value}</th>
        });
        return tableHeaderRow;
    }

    getTableSectionRows(openedReport) {
        let sectionRows = openedReport.rows.filter(i => i.rowType === 'Section');
        let tableSectionRows = sectionRows.map(i => {
            if (i.rows && i.rows.length) {
                let subSectionRows = i.rows.map(j => {
                    let subSectionRowCells = j.cells.map(k => {
                        return <td>{k.value}</td>
                    })
                    return <tr>{subSectionRowCells}</tr>
                })
                return (
                    <React.Fragment>
                        <tr><th>{i.title}</th></tr>
                        {subSectionRows}
                    </React.Fragment>
                )
            }
            else {
                return (
                    <tr><th>{i.title}</th></tr>
                )
            }
        });
        return tableSectionRows;
    }

    render() {
        const reportDateOptions = this.state.reports && this.state.reports.length ? this.state.reports.map(i => {
            return <option value={i.date}>{i.date}</option>
        }) : [];

        return (
            <React.Fragment>
                <div className="dashboard_form">
                    <Form onSubmit={this.handleSubmit}>
                        <Form.Row>
                            <Form.Group as={Col} controlId="reportDate">
                                <Form.Label>Date</Form.Label>
                                <Form.Control
                                    as="select"
                                    value={this.state.reportDate}
                                    onChange={(event) => this.handleOnChange(event, 'reportDate')}>
                                    {reportDateOptions}
                                </Form.Control>
                            </Form.Group>
                            <Form.Group as={Col} controlId="reportTimeFrame">
                                <Form.Label>Timeframe</Form.Label>
                                <Form.Control
                                    disabled
                                    as="select"
                                    value={this.state.reportTimeFrame}
                                    onChange={(event) => this.handleOnChange(event, 'reportTimeFrame')}>
                                    <option value="MONTH">Month</option>
                                    <option value="QUARTER">Quarter</option>
                                    <option value="YEAR">Year</option>
                                </Form.Control>
                            </Form.Group>
                            <Form.Group as={Col} className="margin_top_30">
                                <Button variant="primary" type="submit" onClick={() => this.handleGetReport()}>
                                    Get Report
                                </Button>
                            </Form.Group>
                        </Form.Row>
                        {
                            this.state.openedReport &&
                            <React.Fragment>
                                <h5>{this.state.openedReport.reportTitles[0]} of {this.state.openedReport.reportTitles[1]} {this.state.openedReport.reportTitles[2]}</h5>
                                <Table bordered hover>
                                    <thead>
                                        <tr>
                                            {this.getTableHeaderRow(this.state.openedReport)}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.getTableSectionRows(this.state.openedReport)}
                                    </tbody>
                                </Table>
                            </React.Fragment>
                        }
                    </Form>
                </div>
            </React.Fragment>)
    }
}

export default DashboardComponent;