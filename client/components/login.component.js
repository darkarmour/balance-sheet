import React from "react";
import { Button, Form, FormGroup } from 'react-bootstrap';

class LoginComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',

            emailError: undefined,
            passwordError: undefined,
            userError: undefined
        }
    }

    handleSubmit(event) {
        event.preventDefault();
    }

    handleOnChange(event, id) {
        this.removeError();
        this.setState({ [id]: event.target.value })
    }

    validateEmail(email) {
        const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.test(String(email).toLowerCase());
    }

    async login() {
        this.removeError();
        let email = this.state.email;
        let password = this.state.password;
        if (email && password) {
            let isValidEmail = this.validateEmail(email);
            if (isValidEmail) {
                let result = await this.props.handleLogin(email, password);
                console.log('Result', result);
                if (!result)
                    this.setState({ userError: 'User not found. Try signup' })
            }
            else
                this.setState({ emailError: 'Enter a valid email' })
        }
        else {
            this.setState({
                emailError: email ? undefined : 'Email is required',
                passwordError: password ? undefined : 'Password is required'
            })
        }
    }

    removeError() {
        this.setState({ emailError: undefined, passwordError: undefined, userError: undefined });
    }

    render() {
        return (
            <React.Fragment>
                <div className="login_form">
                    <Form onSubmit={this.handleSubmit}>
                        <Form.Group controlId="email">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control
                                autoFocus
                                autoComplete="new-password"
                                type="email"
                                placeholder="Enter email"
                                value={this.state.email}
                                onChange={(event) => this.handleOnChange(event, 'email')} />
                            {this.state.emailError &&
                                <p className="error_color">{this.state.emailError}</p>}
                        </Form.Group>
                        <Form.Group controlId="password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Password"
                                value={this.state.password}
                                onChange={(event) => this.handleOnChange(event, 'password')} />
                            {this.state.passwordError &&
                                <p className="error_color">{this.state.passwordError}</p>}
                        </Form.Group>
                        <Button variant="primary"
                            type="submit"
                            disabled={this.props.isLoading}
                            onClick={() => this.login()}>
                            Login
                        </Button>{' '}
                        {/* <Button variant="secondary"
                            onClick={() => this.props.toggleSignup()}
                            disabled={this.props.isLoading}>Signup</Button> */}
                        {this.state.userError &&
                            <p className="error_color">{this.state.userError}</p>}
                    </Form>
                </div>
            </React.Fragment>
        )
    }
}



export default LoginComponent;

