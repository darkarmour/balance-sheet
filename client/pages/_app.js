import App from 'next/app';
import Head from 'next/head';
import React from "react";

import '../styles/globals.css'

class MyApp extends App {
    render() {
        const { Component, pageProps } = this.props;
        return (
            <React.Fragment>
                <Head>
                    <title>Balance Sheet | Jayaprakash</title>
                    <link rel="icon" type="image/x-icon" href="thumbnail.png" />
                    <link
                        rel="stylesheet"
                        href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
                        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
                        crossOrigin="anonymous"
                    />
                </Head>
                <Component {...pageProps} />
            </React.Fragment>
        );
    }
}

export default MyApp;
