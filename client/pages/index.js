import React from "react";
import { Container, Row, Col, Breadcrumb, Card, Navbar, Button, Nav } from 'react-bootstrap';

import LoginComponent from '../components/login.component';
import DashboardComponent from '../components/dashboard.component';

import accountApi from '../api/account';
import cache from '../utils/cache';

export default class Home extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      user: undefined,
      isInitialised: undefined,

      isLoading: undefined,
      isSignup: undefined
    };
  }

  componentDidMount() {
    const storedUser = cache.getItem('storedUser');
    console.log('Stored user', storedUser)
    if (storedUser) {
      this.setState({
        user: storedUser
      });
    }
    this.setState({ isInitialised: true });
  };

  async handleLogin(email, password) {
    this.setState({ isLoading: true });
    return accountApi.getAccount(email, password).then(account => {
      if (account && account.email) {
        cache.setItem('storedUser', account);
        this.setState({ isLoading: false, user: account });
        return true;
      }
      else {
        this.setState({ isLoading: false });
        return false
      }
    }).catch(error => {
      console.log('Error while login', error);
      this.setState({ isLoading: false });
      return false;
    });
  }

  async handleSignup(payload) {
    this.setState({ isLoading: true });
    return accountApi.createAccount(payload).then(account => {
      if (account && account.email) {
        cache.setItem('storedUser', account);
        this.setState({ isLoading: false, isSignup: undefined, user: account })
        return true;
      }
      else {
        this.setState({ isLoading: false });
        return false
      }
    }).catch(error => {
      console.log('Error while signup', error);
      this.setState({ isLoading: false });
      return false;
    });
  }

  async handleUpdate(payload) {
    this.setState({ isLoading: true });
    return accountApi.updateAccount(payload).then(account => {
      cache.setItem('storedUser', account);
      this.setState({ isLoading: false, user: account });
      return true;
    }).catch(error => {
      console.log('Error while update', error);
      this.setState({ isLoading: false });
      return false;
    });
  }

  handleSignout() {
    this.setState({ isLoading: true });
    setTimeout(() => {
      cache.removeItem('storedUser');
      this.setState({ isLoading: false, isSignup: undefined, user: undefined });
    }, 2000);
  }

  handleToggleSignup() {
    let isSignup = this.state.isSignup;
    this.setState({ isSignup: !isSignup });
  }

  render() {
    return (
      <Container fluid className="horizontal_no_padding">
        <Navbar className="bg_whitesmoke">
          <Navbar.Brand>Balance Sheet</Navbar.Brand>
          <Navbar.Toggle />
          {
            this.state.isInitialised &&
            <Navbar.Collapse className="justify-content-end">
              {
                this.state.user && this.state.user.email &&
                <React.Fragment>
                  <Navbar.Text className="margin_right_10">
                    Signed in as: <a>{this.state.user.fullName || this.state.user.email}</a>
                  </Navbar.Text>
                  <Button variant="secondary"
                    disabled={this.state.isLoading}
                    onClick={() => this.handleSignout()}>
                    Signout
                </Button>
                </React.Fragment>
              }
              {
                !this.state.user && this.state.isSignup &&
                <Button variant="secondary"
                  disabled={this.state.isLoading}
                  onClick={() => this.handleToggleSignup()}>
                  Login
                </Button>
              }
            </Navbar.Collapse>
          }
        </Navbar>
        {
          this.state.isInitialised && this.state.user &&
          <DashboardComponent
            isLoading={this.state.isLoading}
            handleUpdate={(payload) => this.handleUpdate(payload)}
            user={this.state.user}>
          </DashboardComponent>
        }
        {
          this.state.isInitialised && !this.state.user && !this.state.isSignup &&
          <LoginComponent
            isLoading={this.state.isLoading}
            handleLogin={(email, password) => this.handleLogin(email, password)}
            toggleSignup={() => this.handleToggleSignup()}>
          </LoginComponent>
        }
        {
          this.state.isInitialised && !this.state.user && this.state.isSignup &&
          <DashboardComponent
            isLoading={this.state.isLoading}
            handleSignup={(payload) => this.handleSignup(payload)}>
          </DashboardComponent>
        }
      </Container>
    )
  }
}
