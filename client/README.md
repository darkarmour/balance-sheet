This is a [Next.js](https://nextjs.org/) front end module bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

- Node version - v12.16.1

## Getting Started

First, run the development server:

```bash
npm install
npm run dev
```
