const apiUrl = 'https://balance-sheet-api-jp.herokuapp.com';

module.exports = {
    getAccount: async (email, password) => {
        return fetch(`${apiUrl}/account?email=${email}&password=${password}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => response.json()).then(account => {
            console.log('Get account', account);
            return account;
        }).catch(error => {
            return error;
        })
    },

    createAccount: async (payload) => {
        return fetch(`${apiUrl}/account`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(payload)
        }).then(response => response.json()).then(account => {
            console.log('Create account', account);
            return account;
        }).catch(error => {
            return error;
        })
    },

    updateAccount: async (payload) => {
        return fetch(`${apiUrl}/account`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(payload)
        }).then(response => response.json()).then(account => {
            console.log('Update account', account);
            return account;
        }).catch(error => {
            return error;
        })
    },

    deleteAccount: async (email) => {
        fetch(`${apiUrl}/account?email=${email}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => response.json()).then(account => {
            console.log(account)
        })
    }
}