require("dotenv").config();
import * as bodyParser from "body-parser";
import * as crypto from 'crypto';
import express from "express";
import { Request, Response } from "express";
import { TokenSet } from 'openid-client';
import {
  Item,
  XeroAccessToken,
  XeroClient,
  XeroIdToken
} from "xero-node";
import jwtDecode from 'jwt-decode';

const cors = require('cors')

const session = require("express-session");
var FileStore = require('session-file-store')(session);
const path = require("path");

const dynamodb = require("./helper/dynamodb");

const client_id = process.env.CLIENT_ID;
const client_secret = process.env.CLIENT_SECRET;
const redirectUrl = process.env.REDIRECT_URI;
const scopes = "offline_access openid profile email accounting.transactions accounting.transactions.read accounting.reports.read accounting.journals.read accounting.settings accounting.settings.read accounting.contacts accounting.contacts.read accounting.attachments accounting.attachments.read files files.read assets assets.read projects projects.read payroll.employees payroll.payruns payroll.payslip payroll.timesheets payroll.settings";
// bankfeeds

const xero = new XeroClient({
  clientId: client_id,
  clientSecret: client_secret,
  redirectUris: [redirectUrl],
  scopes: scopes.split(" "),
  state: "imaParam=look-at-me-go",
  httpTimeout: 2000
});

if (!client_id || !client_secret || !redirectUrl) {
  throw Error('Environment Variables not all set - please check your .env file in the project root or create one!')
}

class App {
  public app: express.Application;
  public consentUrl: Promise<string>

  constructor() {
    this.app = express();
    this.config();
    this.routes();
    this.app.set("views", path.join(__dirname, "views"));
    this.app.set("view engine", "ejs");
    this.app.use(express.static(path.join(__dirname, "public")));

    this.consentUrl = xero.buildConsentUrl()
  }

  private config(): void {
    this.app.use(cors())
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use('/webhooks', bodyParser.raw({ type: 'application/json' }));
    this.app.use(bodyParser.json());
  }

  // helpers
  authenticationData(req, _res) {
    return {
      decodedIdToken: req.session.decodedIdToken,
      tokenSet: req.session.tokenSet,
      decodedAccessToken: req.session.decodedAccessToken,
      accessTokenExpires: this.timeSince(req.session.decodedAccessToken),
      allTenants: req.session.allTenants,
      activeTenant: req.session.activeTenant
    }
  }

  timeSince(token) {
    if (token) {
      const timestamp = token['exp']
      const myDate = new Date(timestamp * 1000)
      return myDate.toLocaleString()
    } else {
      return ''
    }
  }

  sleep(ms) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
  };

  verifyWebhookEventSignature(req: Request) {
    let computedSignature = crypto.createHmac('sha256', process.env.WEBHOOK_KEY).update(req.body.toString()).digest('base64');
    let xeroSignature = req.headers['x-xero-signature'];

    if (xeroSignature === computedSignature) {
      console.log('Signature passed! This is from Xero!');
      return true;
    } else {
      // If this happens someone who is not Xero is sending you a webhook
      console.log('Signature failed. Webhook might not be from Xero or you have misconfigured something...');
      console.log(`Got {${computedSignature}} when we were expecting {${xeroSignature}}`);
      return false;
    }
  };

  private routes(): void {
    const router = express.Router();

    router.get("/", async (req: Request, res: Response) => {
      if (req.session.tokenSet) {
        // This reset the session and required data on the xero client after ts recompile
        await xero.setTokenSet(req.session.tokenSet)
        await xero.updateTenants(false)
      }

      try {
        const authData = this.authenticationData(req, res)

        res.render("home", {
          consentUrl: await xero.buildConsentUrl(),
          authenticated: authData
        });
      } catch (e) {
        res.status(res.statusCode);
        res.render("shared/error", {
          consentUrl: await xero.buildConsentUrl(),
          error: e
        });
      }
    });

    router.get("/callback", async (req: Request, res: Response) => {
      try {
        // calling apiCallback will setup all the client with
        // and return the orgData of each authorized tenant
        const tokenSet: TokenSet = await xero.apiCallback(req.url);
        await xero.updateTenants(false)

        console.log('xero.config.state: ', xero.config.state)

        // this is where you can associate & save your
        // `tokenSet` to a user in your Database
        req.session.tokenSet = tokenSet
        if (tokenSet.id_token) {
          const decodedIdToken: XeroIdToken = jwtDecode(tokenSet.id_token)
          req.session.decodedIdToken = decodedIdToken
        }
        const decodedAccessToken: XeroAccessToken = jwtDecode(tokenSet.access_token)
        req.session.decodedAccessToken = decodedAccessToken
        req.session.tokenSet = tokenSet
        req.session.allTenants = xero.tenants
        req.session.activeTenant = xero.tenants[0]

        res.render("callback", {
          consentUrl: await xero.buildConsentUrl(),
          authenticated: this.authenticationData(req, res)
        });
      } catch (e) {
        res.status(res.statusCode);
        res.render("shared/error", {
          consentUrl: await xero.buildConsentUrl(),
          error: e
        });
      }
    });

    router.post("/change_organisation", async (req: Request, res: Response) => {
      try {
        const activeOrgId = req.body.active_org_id
        const picked = xero.tenants.filter((tenant) => tenant.tenantId == activeOrgId)[0]
        req.session.activeTenant = picked
        const authData = this.authenticationData(req, res)

        res.render("home", {
          consentUrl: await xero.buildConsentUrl(),
          authenticated: this.authenticationData(req, res)
        });
      } catch (e) {
        res.status(res.statusCode);
        res.render("shared/error", {
          consentUrl: await xero.buildConsentUrl(),
          error: e
        });
      }
    });

    router.get("/refresh-token", async (req: Request, res: Response) => {
      try {
        const tokenSet = await xero.readTokenSet();
        console.log('token expires in:', tokenSet.expires_in, 'seconds')
        console.log('tokenSet.expires_at:', tokenSet.expires_at, 'milliseconds')
        console.log('Readable expiration:', new Date(tokenSet.expires_at * 1000).toLocaleString())

        if (tokenSet.expires_at * 1000 < Date.now()) {
          console.log('token is currently expired: ', tokenSet)
        } else {
          console.log('tokenSet is not expired!')
        }

        // you can refresh the token using the fully initialized client levereging openid-client
        await xero.refreshToken()

        // or if you already generated a tokenSet and have a valid (< 60 days refresh token),
        // you can initialize an empty client and refresh by passing the client, secret, and refresh_token
        const newXeroClient = new XeroClient()
        const newTokenSet = await newXeroClient.refreshWithRefreshToken(client_id, client_secret, tokenSet.refresh_token)
        const decodedIdToken: XeroIdToken = jwtDecode(newTokenSet.id_token);
        const decodedAccessToken: XeroAccessToken = jwtDecode(newTokenSet.access_token)

        req.session.decodedIdToken = decodedIdToken
        req.session.decodedAccessToken = decodedAccessToken
        req.session.tokenSet = newTokenSet
        req.session.allTenants = xero.tenants
        req.session.activeTenant = xero.tenants[0]

        const authData = this.authenticationData(req, res)

        res.render("home", {
          consentUrl: await xero.buildConsentUrl(),
          authenticated: this.authenticationData(req, res)
        });
      } catch (e) {
        res.status(res.statusCode);
        res.render("shared/error", {
          consentUrl: await xero.buildConsentUrl(),
          error: e
        });
      }
    });

    router.get("/disconnect", async (req: Request, res: Response) => {
      try {
        const updatedTokenSet: TokenSet = await xero.disconnect(req.session.activeTenant.id)
        await xero.updateTenants(false)

        if (xero.tenants.length > 0) {
          const decodedIdToken: XeroIdToken = jwtDecode(updatedTokenSet.id_token);
          const decodedAccessToken: XeroAccessToken = jwtDecode(updatedTokenSet.access_token)
          req.session.decodedIdToken = decodedIdToken
          req.session.decodedAccessToken = decodedAccessToken
          req.session.tokenSet = updatedTokenSet
          req.session.allTenants = xero.tenants
          req.session.activeTenant = xero.tenants[0]
        } else {
          req.session.decodedIdToken = undefined
          req.session.decodedAccessToken = undefined
          req.session.allTenants = undefined
          req.session.activeTenant = undefined
        }
        const authData = this.authenticationData(req, res)

        res.render("home", {
          consentUrl: await xero.buildConsentUrl(),
          authenticated: authData
        });
      } catch (e) {
        res.status(res.statusCode);
        res.render("shared/error", {
          consentUrl: await xero.buildConsentUrl(),
          error: e
        });
      }
    });

    router.post("/webhooks", async (req: Request, res: Response) => {
      console.log("webhook event received!", req.headers, req.body, JSON.parse(req.body));
      this.verifyWebhookEventSignature(req) ? res.status(200).send() : res.status(401).send();
    });

    // ******************************************************************************************************************** ACCOUNTING API

    router.get("/balanceSheet", async (req: Request, res: Response) => {
      try {
        res.render("balanceSheet", {
          consentUrl: await xero.buildConsentUrl(),
          authenticated: this.authenticationData(req, res),
          getBalanceSheetReportTitle: undefined
        });
      } catch (e) {
        res.status(res.statusCode);
        res.render("shared/error", {
          consentUrl: await xero.buildConsentUrl(),
          error: e
        });
      }
    });

    router.get("/balanceSheet/get", async (req: Request, res: Response) => {
      try {
        // GET BALANCE SHEET REPORT
        // optional parameters
        let email = req.query.email.toString();
        const balsheetDate = req.query.date.toString();
        const balsheetPeriods = 6;
        const balsheetTimeframe = "MONTH";
        const balsheetTrackingOptionID1 = undefined;
        const balsheetTrackingOptionID2 = undefined;
        const balsheetStandardLayout = true;
        const balsheetPaymentsOnly = false;
        const getBalanceSheetResponse = await xero.accountingApi.getReportBalanceSheet(req.session.activeTenant.tenantId, balsheetDate, balsheetPeriods, balsheetTimeframe, balsheetTrackingOptionID1, balsheetTrackingOptionID2, balsheetStandardLayout, balsheetPaymentsOnly);
        let account;
        if (getBalanceSheetResponse && getBalanceSheetResponse.body) {
          let report = {
            date: balsheetDate,
            period: balsheetPeriods,
            timeframe: balsheetTimeframe,
            report: (getBalanceSheetResponse && getBalanceSheetResponse.body && getBalanceSheetResponse.body.reports) ? getBalanceSheetResponse.body.reports[0] : undefined
          }
          let keyConditionExpression = {
            'email': email,
            'userType': 'account'
          };
          account = await dynamodb.read("Users", keyConditionExpression);
          if (account && account.email) {
            let reports = account.reports || [];
            let isAlreadyExisting = reports.find(i => i.date === report.date && i.period === report.period && i.timeframe === report.timeframe) ? true : false;
            if (!isAlreadyExisting)
              reports.push(report);
            account.reports = reports;
          }
          else {
            account = {
              'email': email,
              'userType': 'account',
              'password': "testtest",
              'reports': [report]
            }
          }
          let writeResponse = await dynamodb.put("Users", account);
          console.log('Write DB Response', writeResponse);
        }

        console.log('Get Balance Sheet', JSON.stringify(getBalanceSheetResponse.body, null, 2));
        res.render("balanceSheet", {
          consentUrl: await xero.buildConsentUrl(),
          authenticated: this.authenticationData(req, res),
          getBalanceSheetReportTitle: `${getBalanceSheetResponse.body.reports[0].reportTitles.toString()}`
        });
      } catch (e) {
        res.status(res.statusCode);
        res.render("shared/error", {
          consentUrl: await xero.buildConsentUrl(),
          error: e
        });
      }
    })

    router.get("/account", async (req: Request, res: Response) => {
      try {
        let keyConditionExpression = {
          'email': req.query.email.toString(),
          'userType': 'account'
        };
        let account = await dynamodb.read("Users", keyConditionExpression);
        res.send(account)
      } catch (e) {
        res.status(res.statusCode);
        res.render("shared/error", {
          consentUrl: await xero.buildConsentUrl(),
          error: e
        });
      }
    });

    const fileStoreOptions = {}

    this.app.use(session({
      secret: "something crazy",
      store: new FileStore(fileStoreOptions),
      resave: false,
      saveUninitialized: false,
      cookie: { secure: false },
    }));

    this.app.use("/", router);
  }
}

export default new App().app;
