var AWS = require('aws-sdk');
AWS.config.update({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: 'us-east-1'
});
var docClient = new AWS.DynamoDB.DocumentClient();

module.exports = {
    put: async (tableName, item) => {
        let params = {
            TableName: tableName,
            Item: item
        };
        console.log(params)
        try {
            let putResponse = await docClient.put(params).promise();
            return putResponse ? putResponse : undefined;
        }
        catch (error) {
            console.log('Error while put operation in dynamodb', error);
            return undefined;
        }
    },

    read: async (tableName, keyCondition) => {
        let params = {
            TableName: tableName,
            Key: keyCondition
        };
        try {
            let readResponse = await docClient.get(params).promise();
            return (readResponse && readResponse['Item']) ? readResponse['Item'] : {};
        }
        catch (error) {
            console.log('Error while read operation in dynamodb', error);
            return undefined;
        }
    },

    delete: async (tableName, keyCondition) => {
        let params = {
            TableName: tableName,
            Key: keyCondition
        };
        try {
            let deleteResponse = await docClient.delete(params).promise();
            return deleteResponse;
        }
        catch (error) {
            console.log('Error while delete operation in dynamodb', error);
            return undefined;
        }
    },
}